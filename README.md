## Stahování
Kompletní návod na stažení...

```sh
cd desktop (or any directory)
mkdir name_of_folder_youre_creating
cd name_of_folder_youre_creating
git clone https://gitlab.com/24sbronec/itwradovan.git
```

Stažení do vaší (předem vytvořené) složky...

```sh
cd your_folder
git clone https://gitlab.com/24sbronec/itwradovan.git
```
(◠‿◠)/
